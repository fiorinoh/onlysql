\section{Trier}

La problématique du tri consiste à mettre dans l'ordre un ensemble
d'éléments. Il a été démontré qu'un algorithme ne peut pas trier $N$
éléments avec une complexité meilleure que $O(N\log(N))$. Nous
proposons dans cette partie de vous montrer ce qu'est un «~mauvais~»
algorithme de tri, puis de présenter quelques-uns des «~bons~»
algorithmes.


\subsection{Tri par insertion : un mauvais élève}
\index{tri!par insertion}
Le tri par insertion est la méthode que l'on utilise pour trier un jeu
de cartes : on prend la première, et on insère chacune des suivantes à
sa place. Dans le cadre de l'algorithmie, cela revient à chercher à
insérer une valeur parmi une séquence déjà triée. Considérons la
séquence suivante :
\begin{center}
  \includegraphics[scale=.49]{sda-tri-sequence}
\end{center}
On commence par supposer que la première valeur est en place, et on
traite la deuxième, c'est-à-dire qu'on se demande s'il faut la placer
avant ou après la première :
\begin{center}
  \includegraphics[scale=.49]{sda-tri-insertion-ini}
\end{center}
À chaque étape de l'algorithme de tri par insertion, une partie de la
séquence a été triée :
\begin{center}
  \includegraphics[scale=.49]{sda-tri-insertion-0}
\end{center}
On se demande ensuite où l'on va insérer la valeur courante (ici 8) :
\begin{center}
  \includegraphics[scale=.49]{sda-tri-insertion-1}
\end{center}
Une fois l'emplacement  trouvé, on insère la valeur à sa place :
\begin{center}
  \includegraphics[scale=.49]{sda-tri-insertion-2}
\end{center}
Et on reprend le même processus avec la valeur suivante (ici~3) à
insérer :
\begin{center}
\includegraphics[scale=.49]{sda-tri-insertion-3}
\end{center}
Si on considère que les opérations fondamentales sont l'échange et la
comparaison, on peut dire que l'algorithme du tri par insertion est en
$O(N^2)$ dans le pire des cas et en moyenne. Pour avoir un ordre
d'idée du temps qu'implique cette complexité, on peut examiner les
diagrammes de la figure~\vref{fig-sda-tri-insertion-bench}. On notera
que ces courbes s'approchent de paraboles comme prévu par la
complexité théorique.
\begin{figure}[tb]
  \begin{agrandirmarges}{0.5cm}{0.5cm}
  \begin{center}
     \subfloat[jusqu'à 10000 données]{%
       \includegraphics[width=.55\textwidth]{tri-insertion-1-CORE-I5.pdf}}
     \subfloat[jusqu'à 1 million de données]{
       \includegraphics[width=.55\textwidth]{tri-insertion-2-CORE-I5.pdf}}
  \end{center}
  \caption{Temps nécessaire pour le tri par insertion sur un
    processeur Intel Core i5\textsuperscript{\textregistered}. En
    abscisse le nombre de données à traiter, en ordonnée le temps en
    secondes. Les courbes du bas montre le temps nécessaire pour trier
    des données «~presque~» triées.}
  \label{fig-sda-tri-insertion-bench}
\end{agrandirmarges}
\end{figure}
Il est important de noter
(cf. figure~\ref{fig-sda-tri-insertion-bench}) que ce tri est
performant pour des séquences «~presque~» triées. Cette propriété qui
peut être utilisée dans certaines applications, est exploitée dans le
tri Shell (cf.  \S~\ref{sec-tri-shell}). On retiendra que :
\begin{propriete}
  La complexité du tri par insertion est de :
  \begin{itemize}
  \item $O(N^2)$ dans le pire des cas et en moyenne ;
  \item $O(N)$ pour des séquences quasiment triées. 
  \end{itemize}
\end{propriete}


\subsection{Tri Shell}
\label{sec-tri-shell}
\index{tri!Shell}

Le tri Shell doit son nom à D. L. \textsc{Shell} qui l'a conçu en
1959.  L'idée de ce tri repose sur la constatation que le tri par
insertion est efficace si les données sont «~presque triées.~»
\textsc{Shell} a donc proposé d'effectuer un certain nombre de  «
pré-tris~» rapides avant d'effectuer un tri par insertion. 

À ce jour les avis sont partagés sur la complexité du tri Shell. Il y
a en réalité deux \emph{conjectures} pour la complexité en moyenne :
$O(N\log_2N)$ ou $O(N^{1.25})$. Il s'agit d'hypothèses qui n'ont pu
être démontrées mathématiquement.  Nous illustrons le fonctionnement
du tri Shell en étudiant un exemple. Soit la séquence :
\begin{center}
  \includegraphics[scale=.5]{sda-tri-shell-ini}
\end{center}
On trie d'abord les éléments pris tous les $h_1$ (ici 6) en ignorant
tous les autres. On utilise un simple tri par insertion pour effectuer
ce tri :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-shell-0}\\
  \includegraphics[scale=.55]{sda-tri-shell-1}
\end{center}
Même chose, en se décalant d'un cran à droite :
\begin{center}
\includegraphics[scale=.55]{sda-tri-shell-2}\\
  \includegraphics[scale=.55]{sda-tri-shell-3}
\end{center}
Encore un cran vers la droite : ici rien à faire la sous-séquence est
triée. 
\begin{center}
  \includegraphics[scale=.55]{sda-tri-shell-4}
\end{center}
On trie ensuite les éléments pris tous les $h_2$ (ici 3) en ignorant
tous les autres :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-shell-5}
\end{center}
Même chose un cran à droite :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-shell-6}
\end{center}
Même chose un autre cran à droite :
\begin{center}
\includegraphics[scale=.55]{sda-tri-shell-7}\\
\includegraphics[scale=.55]{sda-tri-shell-8}
\end{center}
Enfin, on effectue un tri par insertion ($h_3=1$) :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-shell-9}\\
  \includegraphics[scale=.55]{sda-tri-shell-10}
\end{center}
La suite des $h_i$ introduite par \textsc{Shell} n'est pas celle
présentée ---~par souci de simplicité~---dans l'exemple précédent,
mais :
\begin{center}
  $..., 1093, 364, 121, 40, 13, 4, 1.$
\end{center}
Ou comme diraient les mathématiciens, de façon récurrente :
\begin{displaymath}
  \left\{\begin{array}{ccl}
        h_0&=&1\\
        h_n&=&3h_{n-1}+1
        \end{array}\right. 
\end{displaymath}


\iffalse
\subsection{Tri par tas}
\label{sec-tri-tas}

Le tri par tas est un algorithme s'appuyant sur une structure de
données nommée \emph{Tas} (\anglais{heap} en anglais) ou \emph{file de
  priorité}. On peut représenter schématiquement un tas sous la forme
d'un \voir{sec-recherche-abr}{arbre binaire} particulier :
\begin{center}
  \includegraphics[scale=.4]{sda-tas-filiation}
\end{center}
Dans cet arbre les deux fils de chaque n\oe d ont une priorité
inférieure.  Voici par exemple un tas en considérant que plus une la
valeur d'un élément est proche du début de l'alphabet, plus sa
priorité est importante :
\begin{center}
  \includegraphics[scale=.4]{sda-tas}
\end{center}
Cette structure possède deux fonctionnalités ou primitives :
\begin{enumerate}
\item l'insertion
\item l'extraction de l'élément de priorité maximale
\end{enumerate}
Et le plus important c'est que l'organisation interne d'un tas ---~que
nous allons découvrir dans ce qui suit~--- garantit que ces deux
primitives peuvent être réalisées en un temps logarithmique. Pour
trier des données à l'aide d'un tas, on opère donc en deux étapes :
\begin{enumerate}
\item \emph{insérer} toutes les données à trier dans un tas ;
  \begin{center}
    \includegraphics[width=.25\textwidth]{sda-tas-tri-1}
  \end{center}
\item les \emph{extraire} une par une. 
  \begin{center}
    \includegraphics[width=.25\textwidth]{sda-tas-tri-2}
  \end{center}
\end{enumerate}
Étant donné que les opérations d'insertion et d'extraction sont en
$O(\log_2N)$, et qu'il faut en réaliser $N$ de chaque, on peut dire
que la complexité du tri par tas est $O(N\log_2(N))$ et ce dans le
pire des cas.  \iffalse
\begin{figure}[hbtp]
  \begin{center}
     \subfloat[Duron 800 Mhz]{%
       \includegraphics[width=.4\textwidth]{tri-tas-duron-0_8}}
     \subfloat[Pentium III 1 Ghz]{%
       \includegraphics[width=.4\textwidth]{tri-tas-cel-1_6}}\\
     \subfloat[Celeron 1.6 Ghz]{%
       \includegraphics[width=.4\textwidth]{tri-tas-cel-1_6}}
     \subfloat[Pentium IV 2.4 Ghz]{%
       \includegraphics[width=.4\textwidth]{tri-tas-cel-1_6}}
  \end{center}
  \caption{Temps nécessaire pour le tri par tas sur les quatre
    machines test}
  \label{fig-sda-tri-tas-bench}
\end{figure}
\fi
 
\subsubsection{Insertion dans un tas}

Pour insérer une nouvelle valeur dans un tas, on opère en deux étapes :
\begin{enumerate}
\item On \emph{greffe} l'élément en \emph{bas} de l'arbre ;
\item On le fait \emph{remonter} pour conserver
  l'\emph{intégrité} du tas, en échangeant avec son père si celui-ci
  est de valeur supérieure ;
\end{enumerate}

\noindent\begin{minipage}{.6\textwidth}
Par exemple, pour insérer la valeur \texttt{L} dans le tas exemple du
paragraphe précédent, on le greffe dans ce dernier à la première place
disponible
\end{minipage}%
\begin{minipage}{.4\textwidth}
  \begin{center}
  \includegraphics[scale=.35]{sda-tas-insertion-0}
\end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
On commence ensuite la remontée : on se demande donc si le sommet
nouvellement inséré a une valeur inférieure à celle du sommet
père
\end{minipage}
\begin{minipage}{.4\textwidth}
\begin{center}
  \includegraphics[scale=.35]{sda-tas-insertion-1}
\end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  Ici \texttt{L} a une valeur inférieure à son père~\texttt{T},
  on les échange donc. On réitère ensuite le processus en comparant le
  sommet~\texttt{L} avec son nouveau père~\texttt{M}
\end{minipage}
\begin{minipage}{.4\textwidth}
\begin{center}
  \includegraphics[scale=.35]{sda-tas-insertion-2}
\end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  Le sommet contenant la valeur~\texttt{L} est échangé avec celui
  contenant la valeur~\texttt{M}. Et on réitère le processus en
  comparant~\texttt{L} avec la racine. 
\end{minipage}
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.35]{sda-tas-insertion-3}
  \end{center}
\end{minipage}

Le processus s'arrête ici car \texttt{K} a une valeur inférieure à
\texttt{L}. On comprend aisément que le nouveau
sommet inséré au dernier niveau, remontera dans le pire des cas,
jusqu'à la racine. On parcourra donc au pire la hauteur de l'arbre
quasi complet. On peut donc affirmer que la complexité dans le pire
des cas, et en moyenne, est en $O(\log_2N)$. 

\subsubsection{Extraire la racine}

On procédera en trois étapes :
\begin{enumerate}
\item On extrait la racine du tas qui est nécessairement l'élément de
  priorité max (tous ses descendants ont une priorité inférieure) ;
\item On met le dernier élément à la racine ;
\item On le fait \emph{redescendre} pour conserver l'\emph{intégrité}
  du tas, en procédant à des échanges successifs avec les descendants
  de priorité supérieure.
\end{enumerate}

\noindent\begin{minipage}{.6\textwidth}
  On commence par enlever la racine (c'est l'élément de priorité max) :
\end{minipage}
\begin{minipage}{.4\textwidth}
\begin{center}
  \includegraphics[scale=.35]{sda-tas-extraction-0}
\end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  On met à la place, le sommet le plus à droite du dernier niveau
\end{minipage}
\begin{minipage}{.4\textwidth}
\begin{center}
\includegraphics[scale=.35]{sda-tas-extraction-1}
\end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  On fait ensuite redescendre ce sommet en l'échangeant avec celui de
  ses fils dont la priorité est la plus importante (ici on
  choisira~\texttt{M})
\end{minipage}
\begin{minipage}{.4\textwidth}
\begin{center}
\includegraphics[scale=.35]{sda-tas-extraction-2}
\end{center}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
  On réitère le processus en comparant la valeur~\texttt{R} avec ses
  deux nouveaux fils (\texttt{T} et~\texttt{V}). 
\end{minipage}
\begin{minipage}{.4\textwidth}
  \begin{center}
    \includegraphics[scale=.35]{sda-tas-extraction-3}
  \end{center}
\end{minipage}

Le processus s'arrête car \texttt{T} et~\texttt{V} sont des valeurs
supérieures à~\texttt{R}. Il s'arrêterait de toutes manières en
atteignant le bas de l'arbre maximier. En examinant cet exemple on
comprend aisément que la racine provisoire (ici \texttt{R})
redescendra dans le pire des cas jusqu'en bas de l'arbre. La
complexité de l'opération d'extraction de la racine est donc en $O(\log_2N)$. 
\fi



\subsection{Tri rapide}
\index{tri!rapide}
\index{quick sort}

Le tri rapide ou \emph{quick sort} a été conçu par C. A. R. 
\textsc{Hoare} en 1960. Il est plus difficile à implémenter que le tri
Shell. On notera qu'il donne de bons résultats en moyenne et c'est pour
cela qu'il est souvent implanté dans les bibliothèques de
programmation (la librairie standard du langage~C propose par exemple
une fonction permettant de trier des données avec cet algorithme). 

\subsubsection{Complexité}

\begin{propriete}
  Le tri rapide a une complexité en :
  \begin{itemize}
  \item en $O(N\log_2N)$ en moyenne ;
  \item en $O(N^2)$ dans le pire des cas. 
  \end{itemize}
\end{propriete}
\iffalse
Nous donnons à titre indicatif le temps mis par une implémentation de
cet algorithme sur nos quatre machines test à la
figure~\vref{fig-sda-tri-rapide-bench}. 
\begin{figure}[hbtp]
  \begin{center}
     \subfloat[Duron 800 Mhz]{%
       \includegraphics[width=.4\textwidth]{tri-rapide-duron-0_8}}
     \subfloat[Pentium III 1 Ghz]{%
       \includegraphics[width=.4\textwidth]{tri-rapide-cel-1_6}}\\
     \subfloat[Celeron 1.6 Ghz]{%
       \includegraphics[width=.4\textwidth]{tri-rapide-cel-1_6}}
     \subfloat[Pentium IV 2.4 Ghz]{%
       \includegraphics[width=.4\textwidth]{tri-rapide-cel-1_6}}
  \end{center}
  \caption{Temps nécessaire pour le tri rapide sur les quatre
    machines test}
  \label{fig-sda-tri-rapide-bench}
\end{figure}
\fi

\subsubsection{Principe}

On choisit une valeur \emph{pivot} permettant de \emph{partitionner} la
séquence en deux sous-séquences :
\begin{enumerate}
\item d'un côté les valeurs supérieures au pivot ;
\item de l'autre les valeurs inférieures au pivot. 
\end{enumerate}
Une fois la séquence partitionnée de cette manière, on met la valeur
pivot à sa place :
\begin{center}
  \includegraphics[scale=.44]{sda-tri-rapide-partitionnement}
\end{center}
On réitère le processus sur chaque sous-séquence.  Toute la subtilité
de l'algorithme du tri rapide, réside dans un partitionnement rapide
autour du pivot. Idéalement il faudrait choisir comme pivot \emph{la médiane}
de la séquence traitée, de manière à créer deux sous-séquences de
tailles égales. Cependant le calcul de cette médiane \emph{exigerait de trier
la séquence}, ce que nous sommes précisément en train de faire !  On
prend donc un pivot «~au hasard~».  Voici un exemple de déroulement de
l'algorithme du tri rapide. Soit la séquence :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-rapide-ini}
\end{center}
On choisit comme pivot la dernière valeur de la séquence (ici la
valeur~7\footnote{Ouh le vilain qui a choisi comme par hasard, la médiane de la séquence...}) :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-rapide-0}
\end{center}
On réalise le partitionnement de la séquence autour de la valeur pivot
en effectuant deux \emph{parcours}, l'un de l'avant dernière valeur
vers le début de la séquence, l'autre de la première valeur vers la
fin de la séquence. 
\begin{center}        
  \includegraphics[scale=.55]{sda-tri-rapide-1}
\end{center}


\subsubsection{Partitionnement lors du tri rapide}

\newcommand{\parcoursgd}{%
  \raisebox{-1.5pt}{\includegraphics[scale=.55]{sda-tri-rapide-pgd}}}
\newcommand{\parcoursdg}{%
  \raisebox{-1.5pt}{\includegraphics[scale=.55]{sda-tri-rapide-pdg}}}


Voici comment se déroule le partitionnement. On démarre les deux
parcours :
\begin{enumerate}
\item de la gauche vers la droite (symbolisé par~\parcoursgd) à la
  première valeur de la séquence ;
\item de la droite vers la gauche (symbolisé par~\parcoursdg) à
  l'avant-dernière valeur de la séquence. 
\end{enumerate}
On stoppe les parcours :
\begin{itemize}
\item dès que \parcoursgd{} rencontre une valeur supérieure au pivot~(7) ;
\item dès que \parcoursdg{} rencontre une valeur inférieure au pivot~(7). 
\end{itemize}
Par conséquent dans notre exemple, \parcoursgd{} s'arrête sur~15 qui
est supérieur à~7, et \parcoursdg{} sur~5 qui est inférieur à~7 :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-rapide-2}
\end{center}
On échange les deux valeurs sur lesquelles les parcours se sont
interrompus :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-rapide-2b}
\end{center}
On reprend les deux parcours, qui s'arrêtent ensuite sur :
\begin{itemize}
\item la valeur 10 supérieure au pivot~7 pour \parcoursgd ;
\item la valeur 1 inférieure au pivot~7 pour \parcoursdg ;
\end{itemize}
\begin{center}
\includegraphics[scale=.55]{sda-tri-rapide-3}
\end{center}
On échange alors 10 et 1 :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-rapide-4}
\end{center}
On reprend les parcours \parcoursgd{} et~\parcoursdg{} qui
s'interrompent respectivement sur les valeurs~11 et~3 :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-rapide-5}
\end{center}
Valeurs qu'on échange :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-rapide-6}
\end{center}
On reprend ensuite les parcours qui finissent par se \emph{croiser} :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-rapide-7}
\end{center}
On finit le partitionnement en plaçant le pivot à sa place. Il suffit
pour cela d'échanger la valeur pivot avec la valeur sur laquelle s'est
arrêté le parcours~\parcoursgd{} :
\begin{center}
  \includegraphics[scale=.55]{sda-tri-rapide-8}
\end{center}
On notera qu'après cette étape, l'élément contenant la valeur
pivot~(7) est en place pour ce qui concerne le tri :
\begin{center}
\includegraphics[scale=.55]{sda-tri-rapide-9}        
\end{center}
Le schéma ci-dessus illustre également que le partitionnement a pour but
de mettre chacune des valeurs de la séquence «~du bon côté~» par
rapport à la valeur pivot. Il reste alors à trier chaque sous-partie. 
Ce qui est réalisé en \emph{réitérant} le partitionnement sur les deux
\emph{sous-séquences} :
\begin{center}
\raisebox{-.5\baselineskip}{\includegraphics[scale=.55]{sda-tri-rapide-ssg}}
\quad et \quad
\raisebox{-.5\baselineskip}{\includegraphics[scale=.55]{sda-tri-rapide-ssd}}
\end{center}


\subsection{Pour en finir avec les algorithmes performants}
\label{sec-sda-tri-bons-algos}

Pour se rendre compte de l'efficacité des algorithmes performants de
tri nous proposons d'étudier les courbes de la
figure~\vref{fig-sda-bons-tris} qui montrent les temps nécessaires pour
trier jusqu'à 10 millions de données sur notre machine test (Intel
Core i5). On pourra faire quelques remarques :
\begin{enumerate}
\item le tri rapide porte bien son nom, puisqu'il apparaît comme le
  meilleur des algorithmes performants ;
\item il est intéressant de noter que le tri par insertion demandait
  environ 160 secondes pour traiter 1 million valeurs. La complexité
  de ce tri étant quadratique, le temps mis pour traiter 10~fois plus
  de valeurs sera donc 100 fois plus important. On atteindra donc
  $100\times 160$ secondes, c'est-à-dire pratiquement quatre heures et
  demie ! Nous vous invitons à «~méditer~» sur le rapport entre ces
  quatre heures et la poignée de secondes dont ont besoin le tri Shell
  et le tri rapide, et ce \emph{même si pour de petites quantité de
    données, un utilisateur ne verra pas de différence.}
\item dans le même ordre d'idée, il faut se persuader qu'il n'est pas
  possible de représenter sur un même graphique les trois tris
  (insertion, Shell et rapide), tellement le premier est peu
  performant ;
\begin{figure}[tb]
  \centering
  \includegraphics[width=.6\textwidth]{tri-rapides-CORE-I5}
  \caption{Performances relatives du tri Shell (environ 30 secondes
    pour trier 10 millions de données sur notre machine test) et du
    tri rapide (environ 10 secondes pour la même quantité de
    données). Les artefacts s'expliquent par un processus système lancé
    pendant les tests.}
  \label{fig-sda-bons-tris}
\end{figure}
\item La figure montre également que les courbes de temps sont
  quasiment linéaires, ce qui est le cas de la fonction
  $f(x)=x\log_2(x)$... 
\end{enumerate}



\endinput
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../framabook"
%%% End: 
