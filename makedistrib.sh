#!/bin/bash

# nettoyage
make cleandocs

#
# version papier noir et blanc
#
maitre="onlysql-nb"
sed  "s/version.*]/versionpapier,nb]/" framabook.tex > $maitre.tex
echo $maitre > .maitre
make cleanfigs
make mode=nb figs
pdflatex $(cat .maitre)
make bib
make index
pdflatex $(cat .maitre)
pdflatex $(cat .maitre)
make cleantex
rm -f $maitre.tex
#
# version papier couleur
#
maitre="onlysql-couleur"
sed  "s/version.*]/versionpapier]/" framabook.tex > $maitre.tex
echo $maitre > .maitre
make cleanfigs
make figs
pdflatex $(cat .maitre)
make bib
make index
pdflatex $(cat .maitre)
pdflatex $(cat .maitre)
make cleantex
rm -f $maitre.tex

#
# version en ligne
#
maitre="onlysql-enligne"
sed  "s/version.*]/versionenligne]/" framabook.tex > $maitre.tex
echo $maitre > .maitre
pdflatex $(cat .maitre)
make bib
make index
pdflatex $(cat .maitre)
pdflatex $(cat .maitre)
make cleantex
rm -f $maitre.tex

# reset
echo framabook > .maitre
