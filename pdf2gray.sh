#!/bin/bash
# conversion d'un pdf en niveau de gris
gs -sOutputFile=-      \
   -sDEVICE=pdfwrite               \
   -sColorConversionStrategy=Gray  \
   -dProcessColorModel=/DeviceGray \
   -dCompatibiltyLevel=1.4         \
   -dNOPAUSE                       \
   -dPDFSETTINGS=/printer         \
   -dBATCH $@
